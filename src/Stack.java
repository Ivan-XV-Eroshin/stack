public class Stack<T> {

    Node<T> node;

    Stack(){
        node = null;
    }
    Stack(T element) {
        Node<T> newNode = new Node<>();
        newNode.setElement(element);
        newNode.setPrevious(null);
        node = newNode;
    }
    public void push(T element){
        Node<T> newNode = new Node<>();
        newNode.setElement(element);
        newNode.setPrevious(node);
        node = newNode;
    }

    public T pop(){
        Node<T> oldNode = node;
        node = node.getPrevious();
        return oldNode.getElement();
    }

    public boolean isEmpty(){
        return node != null;
    }
}
